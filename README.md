## SDIF Sound Description Interchange Format
The Sound Description Interchange Format (SDIF) is an established standard for the well-defined and extensible interchange of a variety of sound descriptions including representations of the signal for analysis-synthesis like spectral, sinusoidal, time-domain, or higher-level models, sound descriptors like loudness or fundamental frequency, markers, labels, and statistical models. SDIF consists of a basic data format framework and an extensible set of standard sound descriptions. 

see http://sdif.sourceforge.net/

### Ircam SDIF Library Sources
https://sourceforge.net/projects/sdif/files/
